<?php include("plantilla.php");
include("conexion.php");
$consulta = "SELECT * FROM reserva";
$query = mysqli_query($conexion,$consulta);

?>
<h1 class="text-center">Listado de reservas</h1>

<div class="container-fluid">
   <div class="col-md-12">
        <table class="table table-dark table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>DNI Pasajero</th>
                    <th>Nombre Pasajero</th>
                    <th>Hotel</th>
                    <th>Habitacion</th>
                    <th>Fecha de reserva</th>
                    <th>Fecha de ingreso</th>
                    <th>Fecha de salida</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            <?php
              $i = 0; 
              while($row = mysqli_fetch_assoc($query)){
                 $habitacionQ = "SELECT * FROM habitaciones WHERE id=$row[id]";
                 $habitacion= mysqli_query($conexion,$habitacionQ);
                 $rowH = mysqli_fetch_row($habitacion);
                 $hotelQ=mysqli_query($conexion,"SELECT nombre_h FROM hoteles WHERE id_hotel=$rowH[6]");
                 $hotel = mysqli_fetch_row($hotelQ);
                 $pasajero = mysqli_query($conexion,"SELECT * FROM pasajeros WHERE dni=$row[dni]");
                 $rowP=mysqli_fetch_row($pasajero);
                 $i++;
               ?>
                <tr>
                <td><?php echo $i?></td>
                <td><?php echo $rowP[0]?></td>
                <td><?php echo $rowP[1]?></td>
                <td><?php echo $hotel[0]?></td>
                <td><?php echo $rowH[0] ?></td>
                <td><?php echo $row['fecha_reserva']?></td>
                <td><?php echo $row['fecha_ingreso']?></td>
                <td><?php echo $row['fecha_salida']?></td>
                <td>
                <div class="row">
                    <a class="btn btn-primary btn-sm ml-3" ><i class="fa fa-check-circle"></i></a>
                    <a class="btn btn-danger btn-sm ml-2" onclick="return confirm('Estas seguro que quieres eliminarlo?');" id="eliminar"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
               </tr>
              <?php } ?>  
            </tbody>
        </table>
        </div>
</div>



<?php include("footer.php"); ?>