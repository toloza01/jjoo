<?php 
include("plantilla.php");
include("conexion.php");
$consulta = "SELECT * FROM hoteles";
$query = mysqli_query($conexion,$consulta);
?>
<h1 class="text-center">Listado de hoteles</h1>
<div class="container">
        <table class="table table-dark">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Ciudad</th>
                    <th>Estrellas</th>
                    <th>Administrador</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              <?php 
              while($row = mysqli_fetch_assoc($query)){
                  $query2 = "SELECT nombre FROM administradores WHERE dni_admin = $row[dni_admin]";
                  $admin= mysqli_query($conexion,$query2);
                  $name= mysqli_fetch_assoc($admin);
               ?>
                <tr>
                <td><?php echo $row['id_hotel']?></td>
                <td><?php echo $row['nombre_h']?></td>
                <td><?php echo $row['direccion']?></td>
                <td><?php echo $row['ciudad']?></td>
                <td><?php echo $row['estrellas']?></td>
                <td><?php echo $name['nombre']?></td>
                <td>
                    <div class="row">
                        <a class="btn btn-primary btn-sm ml-3" ><i class="fas fa-pencil-alt"></i></a>
                        <a class="btn btn-danger btn-sm ml-2" onclick="return confirm('Estas seguro que quieres eliminarlo?');" id="eliminar"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
               </tr>
              <?php } ?>  
            </tbody>
        </table>
</div>


<?php include("footer.php"); ?>
