<?php 
include("conexion.php");
include("plantilla.php");

$consulta = "SELECT * FROM hoteles";
$query = mysqli_query($conexion,$consulta);
?>
<div class="container">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-primary">
                <div class="text-center text-white">
                    <h4>Solicitar reserva</h4>
                </div>
            </div>
            
            <div class="card-body">
            <div class="row">
                     <?php if(isset($_SESSION['mensaje'])){ ?>
                     <div class="alert alert-success" role="alert">
                        <?php echo $_SESSION['mensaje']; ?>
                        <?php session_unset() ?>
                     </div>
                     <?php } ?>
                 </div>
                <form action="guardarSolicitud.php" method="post">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dni">DNI</label>
                            <input type="number" class="form-control" name="dni" required placeholder="Ingrese su DNI...">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre Completo</label>
                            <input type="text" class="form-control" name="nombre" required placeholder="Ingrese su nombre...">
                        </div>
                        <div class="form-group">
                            <label for="numero">Numero de contacto</label>
                            <input type="number" class="form-control" name="numero" required placeholder="Ingrese su numero...">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required placeholder="Ingrese su email...">
                        </div>
                     </div> 
                     <div class="col-md-6">
                         <div class="form-group">
                             <label for="ingreso">Fecha de ingreso</label>
                             <input type="datetime-local" name="ingreso" required class="form-control">
                         </div>
                         <div class="form-group">
                            <label for="salida">Fecha de Salida</label>
                            <input type="datetime-local" name="salida" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="hotel">Hoteles disponibles</label>
                            <select class="form-control" name="hotel" id="hotel">
                                <?php while($hotel= $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $hotel['id_hotel'] ?>">
                                 <?php echo $hotel['nombre_h'] ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="habitacion">Habitaciones disponibles</label>
                            <select class="form-control" name="habitacion" id="habitacion">
                            <option value='0'>Seleccionar Habitacion</option>
                            </select>
                        </div>
                     </div>
                </div>
                <div class="form-group">
                 <button type="submit" class="btn btn-primary col-6 offset-3">Enviar</button>
                </div>
            </form>
            </div>
        </div>
      </div>
    </div>

<?php
include("footer.php");
?>

<script>
    $(document).ready(function(){
        //ajax para cambiar select dinamicos
        $('#hotel').change(function(){
            $('#habitacion').find('option').remove().end().append('<option value="nadaxd"></option>').val("xd"); //limpiar option
            $('#hotel option:selected').each(function(){
               id_hotel = $(this).val();
               $.post("ajax/habitaciones.php",{
                   id_hotel:id_hotel
                },function(data){
                    $('#habitacion').html(data); //agregar habitacion de hotel seleccionado
                });
            });
        });
     });
</script>
