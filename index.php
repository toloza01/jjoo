<?php include("plantilla.php") ?>


<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="card mr-3" style="width: 16rem;">
                <img src="img/westin-tokyo.jpg" height="260px" class="card-img-top" alt="...">
                <div class="card-body bg-primary text-white ">
                  <p class="card-text">El mejor hotel de todo tokyo</p>
                </div>
            </div>
            <div class="card mr-3" style="width: 16rem;">
                <img src="img/estadio.jpg" height="260px" class="card-img-top" alt="...">
                <div class="card-body bg-primary text-white ">
                  <p class="card-text">Disfruta tokyo 2020</p>
                </div>
            </div>
            <div class="card mr-3" style="width: 16rem;">
                <img src="img/xd.jpg" height="260px" class="card-img-top" alt="...">
                <div class="card-body bg-primary text-white ">
                  <p class="card-text">Mascotas oficiales de tokyo 2020!</p>
                </div>
            </div>
            <div class="card mr-3 " style="width: 16rem;">
                <img src="img/logo.png" height="260px" class="card-img-top" alt="...">
                <div class="card-body bg-primary text-white ">
                  <p class="card-text ">Mascotas oficiales de tokyo 2020!</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("footer.php"); ?>